
# react-native-blink-receipt

## Getting started

`$ npm install react-native-blink-receipt --save`

### Mostly automatic installation

`$ react-native link react-native-blink-receipt`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-blink-receipt` and add `RNBlinkReceipt.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNBlinkReceipt.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNBlinkReceiptPackage;` to the imports at the top of the file
  - Add `new RNBlinkReceiptPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-blink-receipt'
  	project(':react-native-blink-receipt').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-blink-receipt/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-blink-receipt')
  	```

## Usage
```javascript
import RNBlinkReceipt from 'react-native-blink-receipt';

// TODO: What to do with the module?
RNBlinkReceipt;
```
  