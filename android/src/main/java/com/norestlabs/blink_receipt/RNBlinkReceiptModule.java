package com.norestlabs.blink_receipt;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.modules.core.PermissionAwareActivity;
import com.facebook.react.modules.core.PermissionListener;
import com.microblink.EdgeDetectionConfiguration;
import com.microblink.FrameCharacteristics;
import com.microblink.IntentUtils;
import com.microblink.ReceiptSdk;
import com.microblink.Retailer;
import com.microblink.ScanOptions;
import com.microblink.ScanResults;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

public class RNBlinkReceiptModule extends ReactContextBaseJavaModule implements ActivityEventListener {

    private static final int CAMERA_SCAN_RECEIPT_REQUEST = 61111;

    private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";

    private static final String E_CALLBACK_ERROR = "E_CALLBACK_ERROR";
    private static final String E_CAMERA_IS_NOT_AVAILABLE = "E_CAMERA_IS_NOT_AVAILABLE";
    private static final String E_NO_PRODUCTS = "E_NO_PRODUCTS";
    private static final String E_PERMISSIONS_MISSING = "E_PERMISSION_MISSING";

    private Promise promise;

    RNBlinkReceiptModule(ReactApplicationContext reactContext) {
        super(reactContext);
        ReceiptSdk.sdkInitialize(reactContext);
        reactContext.addActivityEventListener(this);
    }

    @Override
    public String getName() {
        return "RNBlinkReceipt";
    }

    @ReactMethod
    public void scan(final Promise promise) {
        final Activity activity = getCurrentActivity();

        if (activity == null) {
            promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
            return;
        }

        if (!isCameraAvailable(activity)) {
            promise.reject(E_CAMERA_IS_NOT_AVAILABLE, "Camera not available");
            return;
        }

        permissionsCheck(activity, Arrays.asList(Manifest.permission.CAMERA), new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                initiateCamera(activity);
                return null;
            }
        });

        this.promise = promise;
    }

    private void initiateCamera(Activity activity) {
        ScanOptions scanOptions = ScanOptions.newBuilder()
                .retailer(Retailer.UNKNOWN)
                .frameCharacteristics(FrameCharacteristics.newBuilder()
                        .storeFrames(true)
                        .compressionQuality(100)
                        .externalStorage(false)
                        .build())
                .edgeDetectionConfiguration(EdgeDetectionConfiguration.defaults())
                .logoDetection(true)
                .build();

        Intent intent = IntentUtils.cameraScan( activity, scanOptions );

        activity.startActivityForResult( intent, CAMERA_SCAN_RECEIPT_REQUEST );
    }

    private boolean isCameraAvailable(Activity activity) {
        return activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)
                || activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }

    private void permissionsCheck(final Activity activity, final List<String> requiredPermissions, final Callable<Void> callback) {

        List<String> missingPermissions = new ArrayList<>();

        for (String permission : requiredPermissions) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }

        if (!missingPermissions.isEmpty()) {

            ((PermissionAwareActivity) activity).requestPermissions(missingPermissions.toArray(new String[missingPermissions.size()]), 1, new PermissionListener() {

                @Override
                public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
                    if (requestCode == 1) {

                        for (int grantResult : grantResults) {
                            if (grantResult == PackageManager.PERMISSION_DENIED) {
                                promise.reject(E_PERMISSIONS_MISSING, activity.getString(R.string.permissions_rationale));
                                return true;
                            }
                        }

                        try {
                            callback.call();
                        } catch (Exception e) {
                            promise.reject(E_CALLBACK_ERROR, "Unknown error", e);
                        }
                    }

                    return true;
                }
            });

            return;
        }

        // all permissions granted
        try {
            callback.call();
        } catch (Exception e) {
            promise.reject(E_CALLBACK_ERROR, "Unknown error", e);
        }
    }

    @Override
    public void onActivityResult(Activity activity, final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == CAMERA_SCAN_RECEIPT_REQUEST && resultCode == Activity.RESULT_OK) {
            ScanResults brScanResults = data.getParcelableExtra(IntentUtils.DATA_EXTRA);
            if (brScanResults != null) {
                promise.resolve(BlinkReceiptSerialize.objectToWritable(brScanResults));
            } else {
                promise.reject(E_NO_PRODUCTS, activity.getString(R.string.no_products_found_on_receipt));
            }
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
    }
}
