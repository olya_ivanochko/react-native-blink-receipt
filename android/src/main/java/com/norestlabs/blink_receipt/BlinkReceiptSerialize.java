package com.norestlabs.blink_receipt;

import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class BlinkReceiptSerialize {
    private static final String TAG = "BlinkReceiptSerialize";

    /**
     * Converts an Object into a React Native WritableMap.
     *
     * @param object Object
     * @return WritableMap
     */
    public static WritableMap objectToWritable(Object object) {
        WritableMap typeMap;
        Type type = new TypeToken<Map<String, Object>>(){}.getType();
        String jsonString = new Gson().toJson(object);
        typeMap = objectMapToWritable((Map<String, Object>)new Gson().fromJson(jsonString, type));

        return typeMap;
    }

    /**
     * Converts an Object array into a React Native WritableArray.
     *
     * @param array Object[]
     * @return WritableArray
     */
    public static WritableArray objectArrayToWritable(Object[] array) {
        WritableArray writableArray = Arguments.createArray();

        for (Object item : array) {
            writableArray.pushMap(objectToWritable(item));
        }

        return writableArray;
    }

    /**
     * Converts an Object Map into a React Native WritableMap.
     *
     * @param map Map<String, Object>
     * @return WritableMap
     */
    private static WritableMap objectMapToWritable(Map<String, Object> map) {
        WritableMap writableMap = Arguments.createMap();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            final String key = entry.getKey();
            final Object value = entry.getValue();
            if (value == null) {
                writableMap.putNull(key);
            } else {
                if (value instanceof Boolean) {
                    writableMap.putBoolean(key, (Boolean) value);
                } else if (value instanceof Integer) {
                    writableMap.putInt(key, ((Integer) value));
                } else if (value instanceof Long) {
                    writableMap.putDouble(key, ((Long) value).doubleValue());
                } else if (value instanceof Double) {
                    writableMap.putDouble(key, (Double) value);
                } else if (value instanceof Float) {
                    writableMap.putDouble(key, ((Float) value).doubleValue());
                } else if (value instanceof String) {
                    writableMap.putString(key, (String) value);
                } else if (Map.class.isAssignableFrom(value.getClass())) {
                    writableMap.putMap(key, objectMapToWritable((Map<String, Object>)value));
                } else if (List.class.isAssignableFrom(value.getClass())) {
                    List<Object> list = (List<Object>) value;
                    Object[] array = list.toArray(new Object[list.size()]);
                    writableMap.putArray(key, objectArrayToWritable(array));
                } else if (value instanceof Date) {
                    writableMap.putDouble(key, ((Date) value).getTime());
                } else {
                    Log.e(TAG, "buildTypeMap: Cannot convert object of type " + value.getClass());
                    writableMap.putNull(key);
                }
            }
        }

        return writableMap;
    }
}
